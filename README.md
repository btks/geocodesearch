# GeocodeSearch

### About

GeocodeSearch is a package that provides proximity-based searching to
Laravel Applications. It contains a handful of functions to find nearby
models based on addresses and geographical coordinates.

### Installation

1. Define a new repository in your composer.json file

        "repositories": [
            {
                "type": "vcs",
                "url": "https://bitbucket.org/btks/geocodesearch.git"
            }
        ],


2. Add the following to your composer.json file as a required dependency:

        "btks/geocodesearch": "dev-master",
