<?php

namespace MosaicLearning\GeocodeSearch;

use Illuminate\Support\Facades\Facade;

/**
 * Class GeocodeSearchFacade
 * @package MosaicLearning\GeocodeSearch
 */
class GeocodeSearchFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'GeocodeSearch';
    }
}