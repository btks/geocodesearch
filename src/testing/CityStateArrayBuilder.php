<?php

$zipCodes = file(dirname(__FILE__) . '/zip-codes.csv');

$states = [];
$citiesStates = [];

$state = 'MD';

for ($i = 1; $i < count($zipCodes) ; $i++) {
    if (!isset($state)) {
        $rand = rand(1, 100);

        if ($rand % 5 !== 0) {
            continue;
        }
    }

    $zipCode = $zipCodes[$i];

    $zipCode = substr($zipCode, 1);
    $zipCode = substr($zipCode, 0, -1);

    $e = explode('","', $zipCode);

    if (!isset($state)) {
        if (isset($states[$e[2]]) && $states[$e[2]] >= 3) {
            continue;
        } else if (!isset($states[$e[2]])) {
            $states[$e[2]] = 0;
        }

        $states[$e[2]]++;
    }

    if (isset($state) && $e[2] != $state) {
        continue;
    }

    $citiesStates[$e[1]] = $e[2];
}

foreach ($citiesStates as $city => $state) {
    echo "'{$city}' => '{$state}',\n";
}