<?php

namespace MosaicLearning\GeocodeSearch\Testing;

use Illuminate\Support\Facades\Config;

/**
 * Class AddressFaker
 *
 * @package MosaicLearning\GeocodeSearch\Testing
 */
class AddressFaker {

    public static $savedPlaces = [];

    /**
     * @var array
     */
    public $fakerIdeas = [
        'Mosaic Learning Columbia, MD',
        'Johns Hopkins University Baltimore, MD',
        'Inova Fairfax Hospital Fairfax, VA',
        'IKEA Nottingham, MD',
        'Virginia Commonwealth University',
        'Capital One Richmond, VA'
    ];

    /**
     * @var array
     */
    public $citiesInMaryland = [
        'DHS' => 'MD',
        'WALDORF' => 'MD',
        'ABELL' => 'MD',
        'ACCOKEEK' => 'MD',
        'AQUASCO' => 'MD',
        'AVENUE' => 'MD',
        'BARSTOW' => 'MD',
        'BEL ALTON' => 'MD',
        'BENEDICT' => 'MD',
        'BRANDYWINE' => 'MD',
        'BROOMES ISLAND' => 'MD',
        'BRYANS ROAD' => 'MD',
        'BRYANTOWN' => 'MD',
        'BUSHWOOD' => 'MD',
        'CALIFORNIA' => 'MD',
        'CALLAWAY' => 'MD',
        'CHAPTICO' => 'MD',
        'CHARLOTTE HALL' => 'MD',
        'CHELTENHAM' => 'MD',
        'CLEMENTS' => 'MD',
        'COBB ISLAND' => 'MD',
        'COLTONS POINT' => 'MD',
        'COMPTON' => 'MD',
        'DAMERON' => 'MD',
        'DOWELL' => 'MD',
        'DRAYDEN' => 'MD',
        'FAULKNER' => 'MD',
        'GREAT MILLS' => 'MD',
        'HELEN' => 'MD',
        'HOLLYWOOD' => 'MD',
        'HUGHESVILLE' => 'MD',
        'HUNTINGTOWN' => 'MD',
        'INDIAN HEAD' => 'MD',
        'IRONSIDES' => 'MD',
        'ISSUE' => 'MD',
        'LA PLATA' => 'MD',
        'LEONARDTOWN' => 'MD',
        'LEXINGTON PARK' => 'MD',
        'LOVEVILLE' => 'MD',
        'LUSBY' => 'MD',
        'MARBURY' => 'MD',
        'MECHANICSVILLE' => 'MD',
        'MORGANZA' => 'MD',
        'MOUNT VICTORIA' => 'MD',
        'NANJEMOY' => 'MD',
        'NEWBURG' => 'MD',
        'PARK HALL' => 'MD',
        'PATUXENT RIVER' => 'MD',
        'PINEY POINT' => 'MD',
        'POMFRET' => 'MD',
        'PORT REPUBLIC' => 'MD',
        'PORT TOBACCO' => 'MD',
        'PRINCE FREDERICK' => 'MD',
        'RIDGE' => 'MD',
        'ROCK POINT' => 'MD',
        'SAINT INIGOES' => 'MD',
        'SAINT LEONARD' => 'MD',
        'SAINT MARYS CITY' => 'MD',
        'SCOTLAND' => 'MD',
        'SOLOMONS' => 'MD',
        'SUNDERLAND' => 'MD',
        'TALL TIMBERS' => 'MD',
        'VALLEY LEE' => 'MD',
        'WELCOME' => 'MD',
        'WHITE PLAINS' => 'MD',
        'SOUTHERN MD FACILITY' => 'MD',
        'ANNAPOLIS JUNCTION' => 'MD',
        'LANHAM' => 'MD',
        'BELTSVILLE' => 'MD',
        'LAUREL' => 'MD',
        'BLADENSBURG' => 'MD',
        'LOTHIAN' => 'MD',
        'MOUNT RAINIER' => 'MD',
        'NORTH BEACH' => 'MD',
        'BOWIE' => 'MD',
        'BRENTWOOD' => 'MD',
        'CAPITOL HEIGHTS' => 'MD',
        'CHESAPEAKE BEACH' => 'MD',
        'CHURCHTON' => 'MD',
        'CLINTON' => 'MD',
        'OWINGS' => 'MD',
        'RIVERDALE' => 'MD',
        'COLLEGE PARK' => 'MD',
        'FORT WASHINGTON' => 'MD',
        'OXON HILL' => 'MD',
        'SUITLAND' => 'MD',
        'DISTRICT HEIGHTS' => 'MD',
        'TEMPLE HILLS' => 'MD',
        'DEALE' => 'MD',
        'DUNKIRK' => 'MD',
        'FORT GEORGE G MEADE' => 'MD',
        'FRIENDSHIP' => 'MD',
        'FULTON' => 'MD',
        'ANDREWS AIR FORCE BASE' => 'MD',
        'SAVAGE' => 'MD',
        'SHADY SIDE' => 'MD',
        'GALESVILLE' => 'MD',
        'GREENBELT' => 'MD',
        'GLENN DALE' => 'MD',
        'UPPER MARLBORO' => 'MD',
        'HARWOOD' => 'MD',
        'HIGHLAND' => 'MD',
        'WEST RIVER' => 'MD',
        'TRACYS LANDING' => 'MD',
        'HYATTSVILLE' => 'MD',
        'JESSUP' => 'MD',
        'BETHESDA' => 'MD',
        'GLEN ECHO' => 'MD',
        'CHEVY CHASE' => 'MD',
        'CABIN JOHN' => 'MD',
        'OLNEY' => 'MD',
        'BROOKEVILLE' => 'MD',
        'POOLESVILLE' => 'MD',
        'BARNESVILLE' => 'MD',
        'BEALLSVILLE' => 'MD',
        'BOYDS' => 'MD',
        'DICKERSON' => 'MD',
        'ROCKVILLE' => 'MD',
        'POTOMAC' => 'MD',
        'DERWOOD' => 'MD',
        'SANDY SPRING' => 'MD',
        'ASHTON' => 'MD',
        'BRINKLOW' => 'MD',
        'BURTONSVILLE' => 'MD',
        'SPENCERVILLE' => 'MD',
        'CLARKSBURG' => 'MD',
        'DAMASCUS' => 'MD',
        'GERMANTOWN' => 'MD',
        'GAITHERSBURG' => 'MD',
        'WASHINGTON GROVE' => 'MD',
        'MONTGOMERY VILLAGE' => 'MD',
        'KENSINGTON' => 'MD',
        'GARRETT PARK' => 'MD',
        'SUBURB MARYLAND FAC' => 'MD',
        'SILVER SPRING' => 'MD',
        'TAKOMA PARK' => 'MD',
        'ABERDEEN' => 'MD',
        'ABERDEEN PROVING GROUND' => 'MD',
        'ABINGDON' => 'MD',
        'GUNPOWDER' => 'MD',
        'ARNOLD' => 'MD',
        'BALDWIN' => 'MD',
        'BEL AIR' => 'MD',
        'BELCAMP' => 'MD',
        'BENSON' => 'MD',
        'BORING' => 'MD',
        'BROOKLANDVILLE' => 'MD',
        'BUTLER' => 'MD',
        'CHASE' => 'MD',
        'CHURCHVILLE' => 'MD',
        'CLARKSVILLE' => 'MD',
        'COCKEYSVILLE' => 'MD',
        'HUNT VALLEY' => 'MD',
        'CROWNSVILLE' => 'MD',
        'DARLINGTON' => 'MD',
        'DAVIDSONVILLE' => 'MD',
        'DAYTON' => 'MD',
        'EDGEWATER' => 'MD',
        'EDGEWOOD' => 'MD',
        'ELLICOTT CITY' => 'MD',
        'COLUMBIA' => 'MD',
        'FALLSTON' => 'MD',
        'FINKSBURG' => 'MD',
        'FOREST HILL' => 'MD',
        'FORK' => 'MD',
        'FORT HOWARD' => 'MD',
        'FREELAND' => 'MD',
        'GAMBRILLS' => 'MD',
        'GIBSON ISLAND' => 'MD',
        'GLEN ARM' => 'MD',
        'GLEN BURNIE' => 'MD',
        'GLYNDON' => 'MD',
        'HAMPSTEAD' => 'MD',
        'ELKRIDGE' => 'MD',
        'HANOVER' => 'MD',
        'HARMANS' => 'MD',
        'HAVRE DE GRACE' => 'MD',
        'HYDES' => 'MD',
        'JARRETTSVILLE' => 'MD',
        'JOPPA' => 'MD',
        'KINGSVILLE' => 'MD',
        'LINEBORO' => 'MD',
        'LINTHICUM HEIGHTS' => 'MD',
        'LONG GREEN' => 'MD',
        'LUTHERVILLE TIMONIUM' => 'MD',
        'MANCHESTER' => 'MD',
        'MARRIOTTSVILLE' => 'MD',
        'MARYLAND LINE' => 'MD',
        'MAYO' => 'MD',
        'MILLERSVILLE' => 'MD',
        'MONKTON' => 'MD',
        'ODENTON' => 'MD',
        'CROFTON' => 'MD',
        'OWINGS MILLS' => 'MD',
        'PARKTON' => 'MD',
        'PASADENA' => 'MD',
        'PERRY HALL' => 'MD',
        'PERRYMAN' => 'MD',
        'PHOENIX' => 'MD',
        'PYLESVILLE' => 'MD',
        'RANDALLSTOWN' => 'MD',
        'REISTERSTOWN' => 'MD',
        'RIDERWOOD' => 'MD',
        'RIVA' => 'MD',
        'SEVERN' => 'MD',
        'SEVERNA PARK' => 'MD',
        'SIMPSONVILLE' => 'MD',
        'SPARKS GLENCOE' => 'MD',
        'STEVENSON' => 'MD',
        'STREET' => 'MD',
        'UPPERCO' => 'MD',
        'UPPER FALLS' => 'MD',
        'WESTMINSTER' => 'MD',
        'WHITEFORD' => 'MD',
        'WHITE HALL' => 'MD',
        'WHITE MARSH' => 'MD',
        'WOODSTOCK' => 'MD',
        'BALTIMORE' => 'MD',
        'TOWSON' => 'MD',
        'GWYNN OAK' => 'MD',
        'PIKESVILLE' => 'MD',
        'SPARROWS POINT' => 'MD',
        'MIDDLE RIVER' => 'MD',
        'ESSEX' => 'MD',
        'DUNDALK' => 'MD',
        'BROOKLYN' => 'MD',
        'CURTIS BAY' => 'MD',
        'HALETHORPE' => 'MD',
        'CATONSVILLE' => 'MD',
        'PARKVILLE' => 'MD',
        'NOTTINGHAM' => 'MD',
        'ROSEDALE' => 'MD',
        'WINDSOR MILL' => 'MD',
        'ANNAPOLIS' => 'MD',
        'CUMBERLAND' => 'MD',
        'ACCIDENT' => 'MD',
        'BARTON' => 'MD',
        'BITTINGER' => 'MD',
        'BLOOMINGTON' => 'MD',
        'CORRIGANVILLE' => 'MD',
        'ECKHART MINES' => 'MD',
        'ELLERSLIE' => 'MD',
        'FLINTSTONE' => 'MD',
        'FRIENDSVILLE' => 'MD',
        'FROSTBURG' => 'MD',
        'GRANTSVILLE' => 'MD',
        'KITZMILLER' => 'MD',
        'LONACONING' => 'MD',
        'LUKE' => 'MD',
        'MC HENRY' => 'MD',
        'MIDLAND' => 'MD',
        'MIDLOTHIAN' => 'MD',
        'MOUNT SAVAGE' => 'MD',
        'OAKLAND' => 'MD',
        'OLDTOWN' => 'MD',
        'PINTO' => 'MD',
        'RAWLINGS' => 'MD',
        'SPRING GAP' => 'MD',
        'SWANTON' => 'MD',
        'WESTERNPORT' => 'MD',
        'EASTON' => 'MD',
        'BARCLAY' => 'MD',
        'BETHLEHEM' => 'MD',
        'BETTERTON' => 'MD',
        'BOZMAN' => 'MD',
        'CAMBRIDGE' => 'MD',
        'CENTREVILLE' => 'MD',
        'CHESTER' => 'MD',
        'CHESTERTOWN' => 'MD',
        'CHURCH CREEK' => 'MD',
        'CHURCH HILL' => 'MD',
        'CLAIBORNE' => 'MD',
        'CORDOVA' => 'MD',
        'CRAPO' => 'MD',
        'CROCHERON' => 'MD',
        'CRUMPTON' => 'MD',
        'DENTON' => 'MD',
        'EAST NEW MARKET' => 'MD',
        'FEDERALSBURG' => 'MD',
        'FISHING CREEK' => 'MD',
        'GALENA' => 'MD',
        'GOLDSBORO' => 'MD',
        'GRASONVILLE' => 'MD',
        'GREENSBORO' => 'MD',
        'HENDERSON' => 'MD',
        'HILLSBORO' => 'MD',
        'HURLOCK' => 'MD',
        'INGLESIDE' => 'MD',
        'KENNEDYVILLE' => 'MD',
        'MCDANIEL' => 'MD',
        'MADISON' => 'MD',
        'MARYDEL' => 'MD',
        'MASSEY' => 'MD',
        'MILLINGTON' => 'MD',
        'NEAVITT' => 'MD',
        'NEWCOMB' => 'MD',
        'OXFORD' => 'MD',
        'PRESTON' => 'MD',
        'PRICE' => 'MD',
        'QUEEN ANNE' => 'MD',
        'QUEENSTOWN' => 'MD',
        'RHODESDALE' => 'MD',
        'RIDGELY' => 'MD',
        'ROCK HALL' => 'MD',
        'ROYAL OAK' => 'MD',
        'SAINT MICHAELS' => 'MD',
        'SECRETARY' => 'MD',
        'SHERWOOD' => 'MD',
        'STEVENSVILLE' => 'MD',
        'STILL POND' => 'MD',
        'SUDLERSVILLE' => 'MD',
        'TAYLORS ISLAND' => 'MD',
        'TEMPLEVILLE' => 'MD',
        'TILGHMAN' => 'MD',
        'TODDVILLE' => 'MD',
        'TRAPPE' => 'MD',
        'WINGATE' => 'MD',
        'WITTMAN' => 'MD',
        'WOOLFORD' => 'MD',
        'WORTON' => 'MD',
        'WYE MILLS' => 'MD',
        'FREDERICK' => 'MD',
        'ADAMSTOWN' => 'MD',
        'BIG POOL' => 'MD',
        'BOONSBORO' => 'MD',
        'BRADDOCK HEIGHTS' => 'MD',
        'BROWNSVILLE' => 'MD',
        'BRUNSWICK' => 'MD',
        'BUCKEYSTOWN' => 'MD',
        'BURKITTSVILLE' => 'MD',
        'CASCADE' => 'MD',
        'CAVETOWN' => 'MD',
        'CHEWSVILLE' => 'MD',
        'CLEAR SPRING' => 'MD',
        'COOKSVILLE' => 'MD',
        'EMMITSBURG' => 'MD',
        'FAIRPLAY' => 'MD',
        'FUNKSTOWN' => 'MD',
        'GLENELG' => 'MD',
        'GLENWOOD' => 'MD',
        'HAGERSTOWN' => 'MD',
        'HANCOCK' => 'MD',
        'IJAMSVILLE' => 'MD',
        'JEFFERSON' => 'MD',
        'KEEDYSVILLE' => 'MD',
        'KEYMAR' => 'MD',
        'KNOXVILLE' => 'MD',
        'LADIESBURG' => 'MD',
        'LIBERTYTOWN' => 'MD',
        'LISBON' => 'MD',
        'LITTLE ORLEANS' => 'MD',
        'MAUGANSVILLE' => 'MD',
        'MIDDLETOWN' => 'MD',
        'MONROVIA' => 'MD',
        'MOUNT AIRY' => 'MD',
        'MYERSVILLE' => 'MD',
        'NEW MARKET' => 'MD',
        'NEW MIDWAY' => 'MD',
        'NEW WINDSOR' => 'MD',
        'POINT OF ROCKS' => 'MD',
        'ROCKY RIDGE' => 'MD',
        'ROHRERSVILLE' => 'MD',
        'SABILLASVILLE' => 'MD',
        'SAINT JAMES' => 'MD',
        'SHARPSBURG' => 'MD',
        'SMITHSBURG' => 'MD',
        'SYKESVILLE' => 'MD',
        'TANEYTOWN' => 'MD',
        'THURMONT' => 'MD',
        'TUSCARORA' => 'MD',
        'UNION BRIDGE' => 'MD',
        'UNIONVILLE' => 'MD',
        'WALKERSVILLE' => 'MD',
        'WEST FRIENDSHIP' => 'MD',
        'WILLIAMSPORT' => 'MD',
        'WOODBINE' => 'MD',
        'WOODSBORO' => 'MD',
        'SALISBURY' => 'MD',
        'ALLEN' => 'MD',
        'BERLIN' => 'MD',
        'BISHOPVILLE' => 'MD',
        'BIVALVE' => 'MD',
        'CRISFIELD' => 'MD',
        'DEAL ISLAND' => 'MD',
        'EDEN' => 'MD',
        'EWELL' => 'MD',
        'FRUITLAND' => 'MD',
        'GIRDLETREE' => 'MD',
        'HEBRON' => 'MD',
        'LINKWOOD' => 'MD',
        'MANOKIN' => 'MD',
        'MARDELA SPRINGS' => 'MD',
        'MARION STATION' => 'MD',
        'NANTICOKE' => 'MD',
        'NEWARK' => 'MD',
        'OCEAN CITY' => 'MD',
        'PARSONSBURG' => 'MD',
        'PITTSVILLE' => 'MD',
        'POCOMOKE CITY' => 'MD',
        'POWELLVILLE' => 'MD',
        'PRINCESS ANNE' => 'MD',
        'QUANTICO' => 'MD',
        'REHOBETH' => 'MD',
        'SHARPTOWN' => 'MD',
        'SHOWELL' => 'MD',
        'SNOW HILL' => 'MD',
        'STOCKTON' => 'MD',
        'TYASKIN' => 'MD',
        'TYLERTON' => 'MD',
        'UPPER FAIRMOUNT' => 'MD',
        'VIENNA' => 'MD',
        'WESTOVER' => 'MD',
        'WHALEYVILLE' => 'MD',
        'WILLARDS' => 'MD',
        'DELMAR' => 'MD',
        'NORTH EAST' => 'MD',
        'PERRY POINT' => 'MD',
        'PERRYVILLE' => 'MD',
        'PORT DEPOSIT' => 'MD',
        'RISING SUN' => 'MD',
        'WARWICK' => 'MD',
        'CECILTON' => 'MD',
        'CHARLESTOWN' => 'MD',
        'CHESAPEAKE CITY' => 'MD',
        'CHILDS' => 'MD',
        'COLORA' => 'MD',
        'CONOWINGO' => 'MD',
        'EARLEVILLE' => 'MD',
        'ELK MILLS' => 'MD',
        'ELKTON' => 'MD',
        'GEORGETOWN' => 'MD'
    ];

    /**
     * @var array
     */
    public $citiesWithState = [
        'ADJUNTAS' => 'PR',
        'ARECIBO' => 'PR',
        'ST THOMAS' => 'VI',
        'CHRISTIANSTED' => 'VI',
        'AMHERST' => 'MA',
        'HADLEY' => 'MA',
        'HATFIELD' => 'MA',
        'ALBION' => 'RI',
        'BRADFORD' => 'RI',
        'CHARLESTOWN' => 'RI',
        'CANDIA' => 'NH',
        'DEERFIELD' => 'NH',
        'EPPING' => 'NH',
        'ELIOT' => 'ME',
        'YORK BEACH' => 'ME',
        'BIDDEFORD POOL' => 'ME',
        'WHITE RIVER JUNCTION' => 'VT',
        'CORINTH' => 'VT',
        'FAIRLEE' => 'VT',
        'BRISTOL' => 'CT',
        'CANTON' => 'CT',
        'CANTON CENTER' => 'CT',
        'FISHERS ISLAND' => 'NY',
        'BLOOMFIELD' => 'NJ',
        'GLEN RIDGE' => 'NJ',
        'LAKE HIAWATHA' => 'NJ',
        'APO' => 'AP',
        'NEW YORK' => 'NY',
        'ATLASBURG' => 'PA',
        'BUNOLA' => 'PA',
        'CLAIRTON' => 'PA',
        'BEAR' => 'DE',
        'CLAYMONT' => 'DE',
        'NEWARK' => 'DE',
        'WASHINGTON' => 'DC',
        'DULLES' => 'VA',
        'MANASSAS' => 'VA',
        'WALDORF' => 'MD',
        'BEL ALTON' => 'MD',
        'FAULKNER' => 'MD',
        'BEESON' => 'WV',
        'FREEMAN' => 'WV',
        'LASHMEET' => 'WV',
        'ADVANCE' => 'NC',
        'BETHANIA' => 'NC',
        'HAMPTONVILLE' => 'NC',
        'BALLENTINE' => 'SC',
        'BAMBERG' => 'SC',
        'BLAIR' => 'SC',
        'NORCROSS' => 'GA',
        'ALPHARETTA' => 'GA',
        'AUBURN' => 'WA',
        'FLEMING ISLAND' => 'FL',
        'BRANFORD' => 'FL',
        'CALLAHAN' => 'FL',
        'DPO' => 'AA',
        'BAILEYTON' => 'AL',
        'BRIERFIELD' => 'AL',
        'BROOKSIDE' => 'AL',
        'ANTIOCH' => 'TN',
        'AUBURNTOWN' => 'TN',
        'BEECHGROVE' => 'TN',
        'CLARKSDALE' => 'MS',
        'FALKNER' => 'MS',
        'HICKORY FLAT' => 'MS',
        'FINCHVILLE' => 'KY',
        'GOSHEN' => 'KY',
        'SAINT CATHARINE' => 'KY',
        'AMLIN' => 'OH',
        'BRINKHAVEN' => 'OH',
        'HEBRON' => 'OH',
        'ANDERSON' => 'IN',
        'FISHERS' => 'IN',
        'FORTVILLE' => 'IN',
        'CLAWSON' => 'MI',
        'FRANKLIN' => 'MI',
        'HARSENS ISLAND' => 'MI',
        'ADEL' => 'IA',
        'ALDEN' => 'IA',
        'BAXTER' => 'IA',
        'BROOKFIELD' => 'WI',
        'CEDAR GROVE' => 'WI',
        'CHILTON' => 'WI',
        'CAMBRIDGE' => 'MN',
        'CENTER CITY' => 'MN',
        'DUNDAS' => 'MN',
        'BERESFORD' => 'SD',
        'BROOKINGS' => 'SD',
        'CHANCELLOR' => 'SD',
        'ABERCROMBIE' => 'ND',
        'AMENIA' => 'ND',
        'FORT RANSOM' => 'ND',
        'ACTON' => 'MT',
        'CUSTER' => 'MT',
        'HARDIN' => 'MT',
        'ARLINGTON HEIGHTS' => 'IL',
        'CRYSTAL LAKE' => 'IL',
        'DES PLAINES' => 'IL',
        'ARNOLD' => 'MO',
        'BARNHART' => 'MO',
        'BERGER' => 'MO',
        'BONNER SPRINGS' => 'KS',
        'BUCYRUS' => 'KS',
        'DENTON' => 'KS',
        'BANCROFT' => 'NE',
        'BELLEVUE' => 'WA',
        'BRUNO' => 'NE',
        'METAIRIE' => 'LA',
        'BRAITHWAITE' => 'LA',
        'WHITE HALL' => 'AR',
        'MONTICELLO' => 'AR',
        'MOSCOW' => 'AR',
        'ALEX' => 'OK',
        'BRADLEY' => 'OK',
        'EDMOND' => 'OK',
        'AUSTIN' => 'TX',
        'TEXHOMA' => 'TX',
        'ADDISON' => 'TX',
        'ARVADA' => 'CO',
        'AURORA' => 'OR',
        'CHEYENNE' => 'WY',
        'BURNS' => 'WY',
        'HILLSDALE' => 'WY',
        'ABERDEEN' => 'ID',
        'ARBON' => 'ID',
        'CLIFTON' => 'ID',
        'ALPINE' => 'UT',
        'SOUTH JORDAN' => 'UT',
        'CEDAR VALLEY' => 'UT',
        'PHOENIX' => 'AZ',
        'CEDARVALE' => 'NM',
        'MILAN' => 'NM',
        'LOS LUNAS' => 'NM',
        'HENDERSON' => 'NV',
        'BUNKERVILLE' => 'NV',
        'LOS ANGELES' => 'CA',
        'ANAHOLA' => 'HI',
        'CAPTAIN COOK' => 'HI',
        'HAIKU' => 'HI',
        'SANTA RITA' => 'GU',
        'POHNPEI' => 'FM',
        'ANTELOPE' => 'OR',
        'BEAVERTON' => 'OR',
        'FEDERAL WAY' => 'WA',
        'ANCHORAGE' => 'AK',
        'JBER' => 'AK'
    ];

    /**
     * @param string $address
     * @param string $name
     * @return object
     */
    public function addressStrToArray($address = '', $name = '', $coordinates = null) {
        $arr = explode(', ', $address);

        $addr1 = $addr2 = $city = $state = $zip = "";

        if (count($arr) == 5) {
            list($addr1, $addr2, $city, $stateAndZip, $country) = $arr;
        } else if (count($arr) == 4) {
            list($addr1, $city, $stateAndZip, $country) = $arr;
        } else if (count($arr) == 3) {
            list($city, $stateAndZip, $country) = $arr;
        }

        if (isset($stateAndZip)) {
            $e = explode(" ", $stateAndZip);

            $state = $e[0];

            if (isset($e[1])) {
                $zip = $e[1];
            }
        }

        return (object) [
            'name'      => $name,
            'address_1' => $addr1,
            'address_2' => '',
            'city'      => $city,
            'state'     => $state,
            'zip'       => $zip,
            'latitude'  => $coordinates !== null ? $coordinates->lat : 0,
            'longitude' => $coordinates !== null ? $coordinates->lng : 0
        ];
    }

    /**
     * @return object
     * @throws \Error
     * @throws \Exception
     * @throws \TypeError
     */
    public function generate($useSpecificState = false) {
        $address = array_random($this->fakerIdeas);
        $arr = $this->citiesWithState;

        if ($useSpecificState) {
            $arr = $this->citiesInMaryland;
        }

        $city = array_random(array_keys($arr));
        $state = 'MD';

        if (!$useSpecificState) {
            $state = $this->citiesWithState[$city];
        }

        if (count(self::$savedPlaces) < 75 || $useSpecificState) {
            $loc = $this->placesNear($city . ', ' . $state);
            $loc = array_chunk($loc, 5);

            if ($loc) {
                self::$savedPlaces = array_merge(self::$savedPlaces, array_values($loc[0]));
            }
        }

        $location = $this->addressStrToArray();

        if (!empty(self::$savedPlaces)) {
            $key = array_random(array_keys(self::$savedPlaces));

            $loc = self::$savedPlaces[$key];
            unset(self::$savedPlaces[$key]);

            self::$savedPlaces = array_values(self::$savedPlaces);

            $addr = $loc->formatted_address;
            $name = $loc->name;
            $coordinates = $loc->geometry->location;

            $location = $this->addressStrToArray($addr, $name, $coordinates);
        }

        if (empty($location->city) || empty($location->state) || empty($location->address_1)) {
            return $this->generate($useSpecificState);
        }

        return $location;
    }

    /**
     * @param $query
     * @return array
     */
    public function queryPlaces($query) {
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://maps.googleapis.com/maps/api/'
        ]);

        $result = $client->request('GET', 'place/textsearch/json', [
            'query' => [
                'query' => $query,
                'key' => config('geocode_search.google.places_api_key')
            ],

            'verify' => false
        ]);

        $json = $result->getBody()->getContents();
        $json = json_decode($json);

        if ($json === null || !is_object($json) || !isset($json->results)) {
            return [];
        }

        return $json->results;
    }

    /**
     * @param $type
     * @param $q
     * @param $address
     * @return string
     */
    public function generateQuery($type, $q, $address) {
        return implode(' ', [$type, $q, $address]);
    }

    /**
     * @param $address
     * @return array
     */
    public function restaurantsNear($address) {
        return $this->queryPlaces(
            $this->generateQuery('restaurants', 'near', $address)
        );
    }

    /**
     * @param $address
     * @return array
     */
    public function placesNear($address) {
        return $this->queryPlaces(
            $this->generateQuery('places', 'near', $address)
        );
    }

    /**
     * @param $city
     * @param $state
     * @return array
     */
    public function placesIn($city, $state) {
        return $this->queryPlaces(
            $this->generateQuery('places', 'in', ($city . ', ' . $state))
        );
    }

    /**
     * @param $address
     * @return array
     */
    public function parksNear($address) {
        return $this->queryPlaces(
            $this->generateQuery('parks', 'near', $address)
        );
    }

    /**
     * @param $address
     * @return array
     */
    public function shopsNear($address) {
        return $this->queryPlaces(
            $this->generateQuery('shops', 'near', $address)
        );
    }

    /**
     * @param $address
     * @return array
     */
    public function banksNear($address) {
        return $this->queryPlaces(
            $this->generateQuery('banks', 'near', $address)
        );
    }
}