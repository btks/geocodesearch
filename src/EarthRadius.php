<?php

namespace MosaicLearning\GeocodeSearch;

/**
 * Trait EarthRadius
 *
 * @package MosaicLearning\GeocodeSearch
 */
trait EarthRadius {

    /**
     * @var int Earth's Radius in kilometers (km)
     */
    public static $EARTH_RADIUS_KM = 6371;

    /**
     * @var int Earth's Radius in miles (mi)
     */
    public static $EARTH_RADIUS_MI = 3959;

    /**
     * @param $unit the unit required
     * @return int the Earth's Radius in the specified unit
     */
    public static function getEarthRadius($unit) {
        switch ($unit) {
            case GeocodeSearch::$KILOMETERS:
                return self::$EARTH_RADIUS_KM;

            case GeocodeSearch::$MILES:
                return self::$EARTH_RADIUS_MI;

            default:
                return self::$EARTH_RADIUS_KM;
        }
    }
}