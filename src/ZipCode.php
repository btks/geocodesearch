<?php

namespace MosaicLearning\GeocodeSearch;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ZipCode
 *
 * @package MosaicLearning\GeocodeSearch
 */
class ZipCode extends Model {

    /**
     * @var array
     */
    protected $hidden = [
        'id', 'latitude', 'longitude', 'created_at', 'deleted_at'
    ];
}