<?php

namespace MosaicLearning\GeocodeSearch;

use Illuminate\Support\ServiceProvider;

/**
 * Class GeocodeSearchServiceProvider
 * @package MosaicLearning\GeocodeSearch
 */
class GeocodeSearchServiceProvider extends ServiceProvider {

    /**
     *
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->publishes([
            __DIR__ . '/config/geocode_search.php' => config_path('geocode_search.php')
        ]);
    }

    /**
     *
     */
    public function register() {
        $this->app->bind('GeocodeSearch', function() {
            return new \MosaicLearning\GeocodeSearch\GeocodeSearch;
        });
    }
}