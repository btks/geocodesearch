<?php

namespace MosaicLearning\GeocodeSearch;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class GeocodeSearch
 *
 * @package MosaicLearning\GeocodeSearch
 * @author Brandon Smith <bsmith@mosaiclearning.com>
 */
class GeocodeSearch {

    use Unit, EarthRadius;

    /**
     * @param $distance
     * @param $unit
     * @param $locationId
     * @return null
     */
    public static function locationsNear($locationId, $distance, $unit) {
        $location = GeoLocation::where('location_id', $locationId)->first();

        if ($location === null) {
            $modelClass = config('geocode_search.model.class');

            if (!class_exists($modelClass)) {
                return null;
            }

            $location = new $modelClass;
            $location = $location->find($locationId);

            if ($location === null) {
                return null;
            }

            list($addr1, $addr2, $city, $state, $zip) = self::extractAddressPieces($location->toArray());

            $address = self::buildAddress($addr1, $addr2, $city, $state, $zip);

            $coordinates = self::getCoordinatesOfAddress($address);

            if ($coordinates === null) {
                return null;
            }

            $location = GeoLocation::create([
                'latitude' => $coordinates->lat,
                'longitude' => $coordinates->lng,
                'location_id' => $locationId
            ]);
        }

        return self::locationsWithin($distance, $unit, $location->latitude, $location->longitude);
    }

    /**
     * @param $zipCode
     * @param $distance
     * @param $unit
     * @return \Illuminate\Support\Collection|null
     */
    public static function locationsNearZip($zipCode, $distance, $unit) {
        $coordinates = self::getGeoLocationOfZipCode($zipCode);

        return $coordinates !== null ? self::locationsWithin($distance, $unit,
            $coordinates->lat, $coordinates->lng) : null;
    }

    /**
     * @param $city
     * @param $state
     * @param $distance
     * @param $unit
     * @return \Illuminate\Support\Collection|null
     */
    public static function locationsNearCityState($city, $state, $distance, $unit) {
        $coordinates = self::getGeoLocationOfCityState($city, $state);

        return $coordinates !== null ? self::locationsWithin($distance, $unit,
            $coordinates->lat, $coordinates->lng) : null;
    }

    /**
     * @param $address
     * @param $distance
     * @param $unit
     * @return \Illuminate\Support\Collection|null
     */
    public static function locationsNearAddress($address, $distance, $unit) {
        $coordinates = self::getCoordinatesOfAddress($address);

        return $coordinates !== null ? self::locationsWithin($distance, $unit,
            $coordinates->lat, $coordinates->lng) : null;
    }

    /**
     * @param $arr
     * @return array
     */
    public static function extractAddressPieces($arr) {
        $attr = array_pad([], 5, '');

        foreach (($keys = array_keys($arr)) as $key) {
            if ($key == 'addr' || str_contains($key, 'addr')) {
                $index = empty($attr[0]) ? 0 : 1;
            } else if ($key == 'city' || str_contains($key, 'city')) {
                $index = 2;
            } else if ($key == 'state' || str_contains($key, 'state')) {
                $index = 3;
            } else if ($key == 'zip' || str_contains($key, 'zip')) {
                $index = 4;
            }

            if (isset($index)) {
                $attr[$index] = $arr[$key];

                $index = null;
            }
        }

        return $attr;
    }

    /**
     * @param $distance
     * @param $unit
     * @param $latitude
     * @param $longitude
     * @return \Illuminate\Support\Collection
     */
    public static function locationsWithin($distance, $unit, $latitude, $longitude) {
        return DB::table('geo_locations')->select(
            DB::raw(
                "location_id, ( " . self::getEarthRadius($unit) . " * acos( cos( radians({$latitude}) ) * cos( radians( `latitude` ) ) * cos( radians( `longitude` ) 
                - radians({$longitude}) ) + sin( radians({$latitude}) ) * sin( radians( `latitude` ) ) ) ) AS distance"
            )
        )->having('distance', '<=', $distance)->orderBy('distance', 'ASC')->get();
    }

    /**
     * @param string $addr1
     * @param string $addr2
     * @param string $city
     * @param string $state
     * @param string $zipCode
     * @return string
     */
    public static function buildAddress($addr1 = '', $addr2 = '', $city = '', $state = '', $zipCode = '') {
        return trim(implode(' ', [$addr1 . $addr2, $city . ', ' . $state, $zipCode]));
    }

    /**
     * @param $arr
     */
    public static function buildAddressFromArray($arr) {
        $pieces = self::extractAddressPieces($arr);

        list($addr1, $addr2, $city, $state, $zip) = $pieces;

        return self::buildAddress($addr1, $addr2, $city, $state, $zip);
    }

    /**
     * @param $zipCode
     * @return mixed
     */
    public static function getGeoLocationOfZipCode($zipCode) {
        return self::getCoordinatesOfAddress(
            self::buildAddress('', '', '', '', $zipCode)
        );
    }

    /**
     * @param $city
     * @return mixed
     */
    public static function getGeoLocationOfCity($city) {
        return self::getCoordinatesOfAddress(
            self::buildAddress('', '', $city)
        );
    }

    /**
     * @param $state
     * @return mixed
     */
    public static function getGeoLocationOfState($state) {
        return self::getCoordinatesOfAddress(
            self::buildAddress('', '', '', $state)
        );
    }

    /**
     * @param $city
     * @param $state
     * @return mixed
     */
    public static function getGeoLocationOfCityState($city, $state) {
        return self::getCoordinatesOfAddress(
            self::buildAddress('', '', $city, $state)
        );
    }

    /**
     * @param $address
     * @return mixed
     */
    public static function getCoordinatesOfAddress($address) {
        $json = null;

        try {
            $client = new \GuzzleHttp\Client([
                'base_uri' => 'https://maps.googleapis.com/maps/api/'
            ]);

            $result = $client->request('GET', 'geocode/json', [
                'query' => [
                    'address' => $address,
                    'key' => config('geocode_search.google.geocoding_api_key')
                ],

                'verify' => false
            ]);

            $json = $result->getBody()->getContents();
            $json = json_decode($json);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            if (config('geocode_search.exceptions.log') === true) {
                Log::info('An exception has occurred fetching coordinates from Google Geocoding: ' . $e->getMessage());
            }
        }

        if ($json === null || !is_object($json) || !isset($json->results)) {
            return null;
        }

        return count($json->results) > 0 ? $json->results[0]->geometry->location : null;
    }
}