<?php

/**
 * GeocodeSearch Configuration
 */

return [

    /**
     * Configuration information required by GeocodeSearch for the application's Location Model
     */
    'model' => [

        /**
         * The namespace of the Location Model
         */
        'class' => '\App\Models\Location',

        /**
         * The table name of the Location Model
         */
        'table' => 'locations',

        /**
         * The primary key for the Location Model
         */
        'primary' => 'id'

    ],

    /**
     * Configuration information for communicating with the Google API
     */
    'google' => [

        /**
         * Google Geocoding API Key
         */
        'geocoding_api_key' => 'AIzaSyAtxnLx3ZxrBcL4UYut-T7idwIhiwsZASQ',

        /**
         * Google Places API Web Services Key
         */
        'places_api_key'    => 'AIzaSyAtxnLx3ZxrBcL4UYut-T7idwIhiwsZASQ'

    ],

    'exceptions' => [

        /**
         * Log exceptions to the Laravel Log
         */
        'log' => true

    ]

];