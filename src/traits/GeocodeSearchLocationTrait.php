<?php

namespace MosaicLearning\GeocodeSearch\Traits;

/**
 * Trait GeocodeSearchLocationTrait
 *
 * @package MosaicLearning\GeocodeSearch\Traits
 */
trait GeocodeSearchLocationTrait {

    /**
     * @return mixed
     */
    public function geoLocation() {
        return $this->hasOne('\MosaicLearning\GeocodeSearch\GeoLocation');
    }
}