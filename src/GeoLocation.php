<?php

namespace MosaicLearning\GeocodeSearch;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GeoLocation
 *
 * @package MosaicLearning\GeocodeSearch
 */
class GeoLocation extends Model {

    /**
     * @var array
     */
    protected $fillable = [
        'latitude', 'longitude', 'location_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location() {
        return $this->belongsTo(config('geocode_search.model.class'), 'location_id', config('geocode_search.model.primary'));
    }
}