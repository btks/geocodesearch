<?php

namespace MosaicLearning\GeocodeSearch;

/**
 * Class Unit
 *
 * @package MosaicLearning\GeocodeSearch
 */
trait Unit {

    /**
     * @var string abbreviation of kilometers unit
     */
    public static $KILOMETERS = 'km';

    /**
     * @var string abbreviation of miles unit
     */
    public static $MILES = 'mi';

}